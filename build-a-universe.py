#!/usr/bin/env python
import tkinter as tk
import threading
import math
from time import sleep

# Class for creating/managing particles.
#
# Contains particle properties and appearance settings

class Particle():
  def __init__(self, x0=0, y0=0, vx0=0, vy0=0, size=5):
    self.x=x0
    self.y=y0
    self.vx=vx0
    self.vy=vy0
    self.size=size

class Universe():
  def __init__(self, app):
    self.app = app
    self.running = True
  # Gets a value from a specified input box
  
  def getInputValue(self, widget):
    try:
      float(widget.get())
      return float(widget.get())
    except:
      return 0
  
  # Starts the universe at initial conditions
  
  def startUniverse(self, canvas):
    self.running = True
    # Define the particle size
    part1 = Particle(self.getInputValue(self.app.x1), self.getInputValue(self.app.y1), self.getInputValue(self.app.vx1), self.getInputValue(self.app.vy1))
    part2 = Particle(self.getInputValue(self.app.x2), self.getInputValue(self.app.y2), self.getInputValue(self.app.vx2), self.getInputValue(self.app.vy2))

    # Define the initial position of the
    p1 = canvas.create_oval(part1.x,part1.y,part1.x + part1.size,part1.y + part1.size)
    p2 = canvas.create_oval(part2.x,part2.y,part2.x + part2.size,part2.y + part2.size)
    self.thread = threading.Thread(target=self.runUniverse, args=(canvas, part1, part2, p1, p2, self.getInputValue(self.app.clock)))
    self.thread.start()
  
  # Run the Universe
  #
  # part1, part2: Particle Objects
  # p1, p2: Ovals representing particles
  
  def runUniverse(self, canvas, part1, part2, p1, p2, slptime = 0.05):
    while self.running:
      sleep(slptime)
      #calculate acceleration
      p1ax = self.calcAcceleration(part1, part2, 'x')
      p1ay = self.calcAcceleration(part1, part2, 'y')
      p2ax = self.calcAcceleration(part2, part1, 'x')
      p2ay = self.calcAcceleration(part2, part1, 'y')
      
      #calculate new velocity from acceleration
      setattr(part1, 'vx', part1.vx + p1ax)
      setattr(part1, 'vy', part1.vy + p1ay)
      setattr(part2, 'vx', part2.vx + p2ax)
      setattr(part2, 'vy', part2.vy + p2ay)
      
      #move the particles
      canvas.move(p1, part1.vx, part1.vy)
      canvas.move(p2, part2.vx, part2.vy)
      setattr(part1, 'x', part1.x + part1.vx)
      setattr(part1, 'y', part1.y + part1.vy)
      setattr(part2, 'x', part2.x + part2.vx)
      setattr(part2, 'y', part2.y + part2.vy)
      
      #apply friction factor
      setattr(part1, 'vx', part1.vx*self.getInputValue(self.app.friction))
      setattr(part1, 'vy', part1.vy*self.getInputValue(self.app.friction))
      setattr(part2, 'vx', part2.vx*self.getInputValue(self.app.friction))
      setattr(part2, 'vy', part2.vy*self.getInputValue(self.app.friction))
      
      #if the particle moves off the canvas, move it back onto the canvas from the other side
      while part1.x >= canvas.winfo_width():
        setattr(part1, 'x', part1.x - canvas.winfo_width())
        canvas.move(p1, -1*canvas.winfo_width(), 0)
      while part1.x <= 0:
        setattr(part1, 'x', part1.x + canvas.winfo_width())
        canvas.move(p1, canvas.winfo_width(), 0)
      while part1.y >= canvas.winfo_height():
        setattr(part1, 'y', part1.y - canvas.winfo_height())
        canvas.move(p1, 0, -1*canvas.winfo_height())
      while part1.y <= 0:
        setattr(part1, 'y', part1.y + canvas.winfo_height())
        canvas.move(p1, 0, canvas.winfo_height())
      while part2.x >= canvas.winfo_width():
        setattr(part2, 'x', part2.x - canvas.winfo_width())
        canvas.move(p2, -1*canvas.winfo_width(), 0)
      while part2.x <= 0:
        setattr(part2, 'x', part2.x + canvas.winfo_width())
        canvas.move(p2, canvas.winfo_width(), 0)
      while part2.y >= canvas.winfo_height():
        setattr(part2, 'y', part2.y - canvas.winfo_height())
        canvas.move(p2, 0, -1*canvas.winfo_height())
      while part2.y <= 0:
        setattr(part2, 'y', part2.y + canvas.winfo_height())
        canvas.move(p2, 0, canvas.winfo_height())

  # Stops the Universe and clears the canvas of particles
  def stopUniverse(self, canvas):
    self.running = False
    canvas.delete('all')
    
  def calcAcceleration(self, target_particle, other_particle, direction):
    # We're going to start with a really simple inverse distance attraction.
    # Add law generation later
    vec = self.calcDistanceVector(target_particle, other_particle)
    g1 = self.getInputValue(self.app.grav1)
    g1p = self.getInputValue(self.app.grav1p)
    g2 = self.getInputValue(self.app.grav2)
    g2p = self.getInputValue(self.app.grav2p)
    g3 = self.getInputValue(self.app.grav3)
    g3p = self.getInputValue(self.app.grav3p)
    if vec['r'] >= self.getInputValue(self.app.null) and vec[direction] != 0:
      return (g1/vec['r']**g1p + g2/vec['r']**g2p + g3/vec['r']**g3p)*(vec[direction])
    else:
      return 0
  
  #Calculate distance vector
  # This calculates a vector which contains the total disance
  # (magnitude), along with the x and y companents
  
  def calcDistanceVector(self, target_particle, other_particle):
    #Get vector magnitude
    x = target_particle.x - other_particle.x
    y = target_particle.y - other_particle.y
    r = math.sqrt(x**2 + y**2)
    unitx = x/r
    unity = y/r
    return {'r':r,'x':x,'y':y}
  

# The main application

class Application(tk.Frame):
  # Set the application to 'running' and create widgets
  def __init__(self, master=None):
    tk.Frame.__init__(self, master)
    self.grid(sticky=tk.N+tk.S+tk.E+tk.W)
    self.running = True
    self.createWidgets()
    self.uni = Universe(self)

  # Create the layout
  def createWidgets(self):
    #Create the viewing field
    for i in range(0,27):
      self.rowconfigure(i, weight=1, minsize=20)
    for i in range(0,6):
      self.columnconfigure(i, weight=1, minsize=100)
    
    #Inputs - str variables
    s1x = tk.StringVar()
    s2x = tk.StringVar()
    s1y = tk.StringVar()
    s2y = tk.StringVar()
    s1vx = tk.StringVar()
    s2vx = tk.StringVar()
    s1vy = tk.StringVar()
    s2vy = tk.StringVar()

    #Inputs - position labels
    self.l1x = tk.Label(self, text='x1')
    self.l1y = tk.Label(self, text='y1')
    self.l2x = tk.Label(self, text='x2')
    self.l2y = tk.Label(self, text='y2')
    self.l1x.grid(column=0, row=22)
    self.l1y.grid(column=1, row=22)
    self.l2x.grid(column=2, row=22)
    self.l2y.grid(column=3, row=22)

    #Inputs - position settings
    self.x1 = tk.Entry(self, textvariable=s1x)
    self.y1 = tk.Entry(self, textvariable=s1y)
    self.x2 = tk.Entry(self, textvariable=s2x)
    self.y2 = tk.Entry(self, textvariable=s2y)
    self.x1.grid(column=0, row=23, sticky=tk.N+tk.S+tk.E+tk.W)
    self.y1.grid(column=1, row=23, sticky=tk.N+tk.S+tk.E+tk.W)
    self.x2.grid(column=2, row=23, sticky=tk.N+tk.S+tk.E+tk.W)
    self.y2.grid(column=3, row=23, sticky=tk.N+tk.S+tk.E+tk.W)

    #Inputs - velocity labels
    self.l1vx = tk.Label(self, text='vx1')
    self.l1vy = tk.Label(self, text='vy1')
    self.l2vx = tk.Label(self, text='vx2')
    self.l2vy = tk.Label(self, text='vy2')
    self.l1vx.grid(column=0, row=24)
    self.l1vy.grid(column=1, row=24)
    self.l2vx.grid(column=2, row=24)
    self.l2vy.grid(column=3, row=24)

    #Inputs - velocity settings
    self.vx1 = tk.Entry(self, textvariable=s1vx)
    self.vy1 = tk.Entry(self, textvariable=s1vy)
    self.vx2 = tk.Entry(self, textvariable=s2vx)
    self.vy2 = tk.Entry(self, textvariable=s2vy)
    self.vx1.grid(column=0, row=25, sticky=tk.N+tk.S+tk.E+tk.W)
    self.vy1.grid(column=1, row=25, sticky=tk.N+tk.S+tk.E+tk.W)
    self.vx2.grid(column=2, row=25, sticky=tk.N+tk.S+tk.E+tk.W)
    self.vy2.grid(column=3, row=25, sticky=tk.N+tk.S+tk.E+tk.W)
    
    #Inputs - Other settings
    clock = tk.StringVar()
    self.lclock = tk.Label(self, text='clock speed')
    self.lclock.grid(column=4, row=22)
    self.clock = tk.Entry(self, textvariable=clock)
    self.clock.grid(column=4, row=23, sticky=tk.N+tk.S+tk.E+tk.W)
    
    self.activeLabel = tk.Label(self, text='Initialization Settings', font=("bold"))
    self.activeLabel.grid(column=1, columnspan=3, row=21)
    self.activeLabel = tk.Label(self, text='Active Settings', font=("bold"))
    self.activeLabel.grid(column=6, row=0)
    
    null = tk.StringVar()
    self.lnull = tk.Label(self, text='null radius')
    self.lnull.grid(column=6, row=1)
    self.null = tk.Entry(self, textvariable=null)
    self.null.grid(column=6, row=2, sticky=tk.N+tk.S+tk.E+tk.W)
    
    friction = tk.StringVar()
    self.lfriction = tk.Label(self, text='friction factor')
    self.lfriction.grid(column=6, row=3)
    self.friction = tk.Entry(self, textvariable=friction)
    self.friction.grid(column=6, row=4, sticky=tk.N+tk.S+tk.E+tk.W)
    
    grav1 = tk.StringVar()
    self.lgrav1 = tk.Label(self, text='1/r^a constant')
    self.lgrav1.grid(column=6, row=5)
    self.grav1 = tk.Entry(self, textvariable=grav1)
    self.grav1.grid(column=6, row=6, sticky=tk.N+tk.S+tk.E+tk.W)
    
    grav1p = tk.StringVar()
    self.lgrav1p = tk.Label(self, text='a')
    self.lgrav1p.grid(column=6, row=7)
    self.grav1p = tk.Entry(self, textvariable=grav1p)
    self.grav1p.grid(column=6, row=8, sticky=tk.N+tk.S+tk.E+tk.W)
    
    grav2 = tk.StringVar()
    self.lgrav2 = tk.Label(self, text='1/r^b constant')
    self.lgrav2.grid(column=6, row=9)
    self.grav2 = tk.Entry(self, textvariable=grav2)
    self.grav2.grid(column=6, row=10, sticky=tk.N+tk.S+tk.E+tk.W)
    
    grav2p = tk.StringVar()
    self.lgrav2p = tk.Label(self, text='b')
    self.lgrav2p.grid(column=6, row=11)
    self.grav2p = tk.Entry(self, textvariable=grav2p)
    self.grav2p.grid(column=6, row=12, sticky=tk.N+tk.S+tk.E+tk.W)
    
    grav3 = tk.StringVar()
    self.lgrav3 = tk.Label(self, text='1/r^c constant')
    self.lgrav3.grid(column=6, row=13)
    self.grav3 = tk.Entry(self, textvariable=grav3)
    self.grav3.grid(column=6, row=14, sticky=tk.N+tk.S+tk.E+tk.W)
    
    grav3p = tk.StringVar()
    self.lgrav3p = tk.Label(self, text='c')
    self.lgrav3p.grid(column=6, row=15)
    self.grav3p = tk.Entry(self, textvariable=grav3p)
    self.grav3p.grid(column=6, row=16, sticky=tk.N+tk.S+tk.E+tk.W)
    
    # Set default values
    clock.set(0.01)
    friction.set(1)
    null.set(10)
    grav1.set(-1)
    grav1p.set(2)
    grav2.set(1000)
    grav2p.set(4)
    s1x.set(250)
    s1y.set(150)
    s2x.set(300)
    s2y.set(150)
    
    #Viewing Area
    self.viewWindow = tk.Canvas(self, bg='#FFFFFF')
    self.viewWindow.grid(column=0, columnspan=6, row=0, rowspan=20, sticky=tk.N+tk.S+tk.E+tk.W)
    #Start Button
    self.startButton = tk.Button(self,text='Start', command=lambda:self.uni.startUniverse(self.viewWindow))
    self.startButton.grid(column=2, row=26, columnspan=3, sticky=tk.N+tk.S+tk.E+tk.W)
    self.stopButton = tk.Button(self,text='Stop', command=lambda:self.uni.stopUniverse(self.viewWindow))
    self.stopButton.grid(column=2, row=27, columnspan=3, sticky=tk.N+tk.S+tk.E+tk.W)
    
    

root=tk.Tk()
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
app = Application(root)
app.master.title('Build a Universe!')
app.mainloop()
